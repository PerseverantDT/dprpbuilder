package io.github.perseverantdt;

import java.io.*;
import java.nio.file.Files;
import javax.swing.*;

import de.marhali.json5.Json5OptionsBuilder;
import de.marhali.json5.stream.Json5Lexer;
import io.github.perseverantdt.config.ProgramConfigs;
import io.github.perseverantdt.core.DatapackBuilder;
import io.github.perseverantdt.core.ResourcePackBuilder;
import io.github.perseverantdt.util.Constants;

public class Main {
    public static void main(String[] args) throws IOException {
        JFrame frame = new JFrame();

        File configFile = Constants.configFilePath.toFile();
        if (!configFile.exists()) {
            int choice = JOptionPane.showConfirmDialog(frame, "Config file not found. Create new config file?", "Config file not found", JOptionPane.YES_NO_OPTION);
            if (choice == JOptionPane.YES_OPTION) {
                InputStream stream = Main.class.getResourceAsStream("/default.json5");
                assert stream != null;

                configFile.createNewFile();
                OutputStream out = new FileOutputStream(configFile);

                stream.transferTo(out);

                stream.close();
                out.close();

                JOptionPane.showMessageDialog(frame, String.format("Config file created at %s. Remember to edit it before rerunning the program.", Constants.configFilePath), "Config file created", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            } else {
                System.exit(1);
            }
        }

        Json5Lexer configLexer = new Json5Lexer(new FileReader(configFile), new Json5OptionsBuilder().prettyPrinting().build());
        ProgramConfigs configs = ProgramConfigs.Parser.parse(configLexer);

        if (!configs.getGeneralConfigs().getOutputPath().toFile().exists()) {
            configs.getGeneralConfigs().getOutputPath().toFile().mkdirs();
        }

        if (Files.isDirectory(configs.getDatapackConfigs().getSourcePath())) {
            try {
                DatapackBuilder.instance.build(configs);
                JOptionPane.showMessageDialog(frame, String.format("Successfully built datapack %s", configs.getDatapackConfigs().getName()), "Successfully built datapack", JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                JOptionPane.showMessageDialog(frame, String.format("Failed to build resource pack\n%s", sw.toString()), "Failed to build resource pack", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(frame, String.format("Data folder %s does not exist or is not a directory. Datapack building will be skipped.", configs.getDatapackConfigs().getSourcePath()), "Invalid data folder", JOptionPane.ERROR_MESSAGE);
        }

        if (Files.isDirectory(configs.getResourcePackConfigs().getSourcePath())) {
            try {
                ResourcePackBuilder.instance.build(configs);
                JOptionPane.showMessageDialog(frame, String.format("Successfully built resource pack %s", configs.getResourcePackConfigs().getName()), "Successfully built resource pack", JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                JOptionPane.showMessageDialog(frame, String.format("Failed to build resource pack\n%s", sw.toString()), "Failed to build resource pack", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(frame, String.format("Assets folder %s does not exist or is not a directory. Resource pack building will be skipped.", configs.getResourcePackConfigs().getSourcePath()), "Invalid data folder", JOptionPane.ERROR_MESSAGE);
        }

        frame.dispose();
    }
}