package io.github.perseverantdt.util;

import com.github.zafarkhaja.semver.Version;
import org.javatuples.Pair;

public class PackFormat {
    static final Pair<String, Integer>[] datapackFormats = new Pair[]{
        new Pair<>("1.13 - 1.14.4", 4),
        new Pair<>("1.15 - 1.16.1", 5),
        new Pair<>("1.16.2 - 1.16.5", 6),
        new Pair<>("1.17 - 1.17.1", 7),
        new Pair<>("1.18 - 1.18.1", 8),
        new Pair<>("1.18.2", 9),
        new Pair<>("1.19 - 1.19.3", 10),
        new Pair<>("1.19.4", 12)
    };

    static final Pair<String, Integer>[] resourcePackFormats = new Pair[] {
        new Pair<>("1.6.1 - 1.8.9", 1),
        new Pair<>("1.9 - 1.10.2", 2),
        new Pair<>("1.11 - 1.12.2", 3),
        new Pair<>("1.13 - 1.14.4", 4),
        new Pair<>("1.15 - 1.16.1", 5),
        new Pair<>("1.16.2 - 1.16.5", 6),
        new Pair<>("1.17 - 1.17.1", 7),
        new Pair<>("1.18 - 1.18.2", 8),
        new Pair<>("1.19 - 1.19.2", 9),
        new Pair<>("1.19.3", 12),
        new Pair<>("1.19.4", 13)
    };

    public static int getDatapackFormat(Version version) {
        for (Pair<String, Integer> pair : datapackFormats) {
            if (version.satisfies(pair.getValue0())) return pair.getValue1();
        }
        return -1;
    }

    public static int getResourcePackFormat(Version version) {
        for (Pair<String, Integer> pair : resourcePackFormats) {
            if (version.satisfies(pair.getValue0())) return pair.getValue1();
        }
        return -1;
    }
}
