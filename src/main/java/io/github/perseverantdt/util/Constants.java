package io.github.perseverantdt.util;

import java.nio.file.Path;

import com.github.zafarkhaja.semver.Version;

public class Constants {
    public static final Version latestMinecraftVersion = Version.valueOf("1.19.4");
    public static final Path configFilePath = Path.of("dprpbuilder.json5");


}
