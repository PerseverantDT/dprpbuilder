package io.github.perseverantdt.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.github.zafarkhaja.semver.Version;
import de.marhali.json5.Json5Object;
import io.github.perseverantdt.config.ProgramConfigs;
import io.github.perseverantdt.util.PackFormat;

public class ResourcePackBuilder extends BaseBuilder {
    public static ResourcePackBuilder instance = new ResourcePackBuilder();

    @Override
    Json5Object createMetaFile(String description, Version mcVersion) {
        Json5Object pack = new Json5Object();
        pack.addProperty("description", description);
        pack.addProperty("pack_format", PackFormat.getResourcePackFormat(mcVersion));

        Json5Object meta = new Json5Object();
        meta.add("pack", pack);

        return meta;
    }

    public void build(ProgramConfigs configs) throws IOException {
        List<Path> filesToAdd = new ArrayList<>();

        StringBuilder sb = new StringBuilder();
        sb.append(configs.getResourcePackConfigs().getName());
        if (configs.getGeneralConfigs().getBuildVersion() != null) {
            sb.append(" v").append(configs.getGeneralConfigs().getBuildVersion().toString());
        }
        if (configs.getGeneralConfigs().mustBuildAsZipped()) {
            sb.append(".zip");
        }

        try (Stream<Path> paths = Files.walk(configs.getResourcePackConfigs().getSourcePath())) {
            paths.filter(Files::isRegularFile).filter(path -> {
                String[] excludes = configs.getGeneralConfigs().getExcludePatterns();
                for (String exclude : excludes) {
                    if (path.toString().matches(exclude)) {
                        return false;
                    }
                }
                return true;
            }).forEach(filesToAdd::add);
        }

        String outputName = sb.toString();
        Path outputPath = configs.getGeneralConfigs().getOutputPath().resolve(outputName);

        if (configs.getGeneralConfigs().mustBuildAsZipped()) {
            compileToZip(filesToAdd, outputPath, configs.getGeneralConfigs(), configs.getResourcePackConfigs(), "assets");
        } else {
            copyToFolder(filesToAdd, outputPath, configs.getGeneralConfigs(), configs.getResourcePackConfigs(), "assets");
        }
    }

    private ResourcePackBuilder(){}
}
