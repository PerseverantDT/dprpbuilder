package io.github.perseverantdt.core;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.github.zafarkhaja.semver.Version;
import de.marhali.json5.Json5Object;
import io.github.perseverantdt.config.GeneralProgramConfigs;
import io.github.perseverantdt.config.SpecificProgramConfigs;

abstract class BaseBuilder {
    abstract Json5Object createMetaFile(String description, Version mcVersion);

    void copyToFolder(List<Path> pathsToCopy, Path outputFolder, GeneralProgramConfigs generalConfigs, SpecificProgramConfigs configs, String baseFolder) throws IOException {
        if (Files.notExists(outputFolder)) Files.createDirectories(outputFolder);

        Json5Object meta = createMetaFile(configs.getDescription(), generalConfigs.getTargetVersion());
        Files.write(outputFolder.resolve("pack.mcmeta"), meta.toString().getBytes());

        if (configs.getIconPath() != null && Files.exists(configs.getIconPath())) {
            Files.copy(configs.getIconPath(), outputFolder.resolve("pack.png"), StandardCopyOption.REPLACE_EXISTING);
        }

        if (configs.getReadmePath() != null && Files.exists(configs.getReadmePath())) {
            Files.copy(configs.getReadmePath(), outputFolder.resolve("README"), StandardCopyOption.REPLACE_EXISTING);
        }

        if (configs.getLicensePath() != null && Files.exists(configs.getLicensePath())) {
            Files.copy(configs.getLicensePath(), outputFolder.resolve("LICENSE"), StandardCopyOption.REPLACE_EXISTING);
        }

        outputFolder = outputFolder.resolve(baseFolder);

        for (Path path : pathsToCopy) {
            Path targetPath = outputFolder.resolve(configs.getSourcePath().relativize(path));
            Files.createDirectories(targetPath.getParent());
            Files.copy(path, targetPath, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    void compileToZip(List<Path> pathsToCompile, Path outputPath, GeneralProgramConfigs generalConfigs, SpecificProgramConfigs configs, String baseFolder) throws IOException {
        Files.deleteIfExists(outputPath);
        Files.createFile(outputPath);

        OutputStream outputStream = Files.newOutputStream(outputPath);
        ZipOutputStream output = new ZipOutputStream(outputStream);
        output.setLevel(9);

        Json5Object meta = createMetaFile(configs.getDescription(), generalConfigs.getTargetVersion());
        output.putNextEntry(new ZipEntry("pack.mcmeta"));
        output.write(meta.toString().getBytes());
        output.closeEntry();

        if (configs.getIconPath() != null && Files.exists(configs.getIconPath())) {
            output.putNextEntry(new ZipEntry("pack.png"));
            Files.copy(configs.getIconPath(), output);
            output.closeEntry();
        }

        if (configs.getReadmePath() != null && Files.exists(configs.getReadmePath())) {
            output.putNextEntry(new ZipEntry("README"));
            Files.copy(configs.getReadmePath(), output);
            output.closeEntry();
        }

        if (configs.getLicensePath() != null && Files.exists(configs.getLicensePath())) {
            output.putNextEntry(new ZipEntry("LICENSE"));
            Files.copy(configs.getLicensePath(), output);
            output.closeEntry();
        }

        for (Path path : pathsToCompile) {
            Path filePath = Path.of(baseFolder).resolve(configs.getSourcePath().relativize(path));
            output.putNextEntry(new ZipEntry(filePath.toString().replace(File.separatorChar, '/')));
            Files.copy(path, output);
            output.closeEntry();
        }

        output.close();
    }
}
