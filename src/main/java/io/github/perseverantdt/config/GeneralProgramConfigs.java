package io.github.perseverantdt.config;

import java.nio.file.Path;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.github.zafarkhaja.semver.Version;
import de.marhali.json5.Json5Array;
import de.marhali.json5.Json5Element;
import de.marhali.json5.exception.Json5Exception;
import de.marhali.json5.stream.Json5Lexer;
import io.github.perseverantdt.exception.InvalidDataException;
import io.github.perseverantdt.util.Constants;

public class GeneralProgramConfigs {
    @NotNull Version targetVersion;
    @Nullable Version buildVersion;
    @NotNull Path outputPath;
    boolean buildAsZipped;
    @NotNull String[] excludePatterns;

    public @NotNull Version getTargetVersion() {
        return targetVersion;
    }

    public void setTargetVersion(Version targetVersion) {
        if (targetVersion == null) {
            throw new IllegalArgumentException("targetVersion cannot be null");
        }
        this.targetVersion = targetVersion;
    }

    public @Nullable Version getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(@Nullable Version buildVersion) {
        this.buildVersion = buildVersion;
    }

    public @NotNull Path getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(@NotNull Path outputPath) {
        this.outputPath = outputPath;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = Path.of(outputPath);
    }

    public boolean mustBuildAsZipped() {
        return buildAsZipped;
    }

    public void setBuildAsZipped(boolean buildAsZipped) {
        this.buildAsZipped = buildAsZipped;
    }

    public @NotNull String[] getExcludePatterns() {
        return excludePatterns;
    }

    public void setExcludePatterns(@NotNull String[] excludePatterns) {
        this.excludePatterns = excludePatterns;
    }

    public GeneralProgramConfigs() {
        targetVersion = Constants.latestMinecraftVersion;
        buildVersion = null;
        outputPath = Path.of("build");
        buildAsZipped = false;
        excludePatterns = new String[0];
    }

    public static class Parser {
        public static GeneralProgramConfigs parse(Json5Lexer lexer) {
            char control;
            String memberName;
            Json5Element value;
            boolean hasTarget = false;
            boolean hasOutput = false;

            control = lexer.nextClean();
            if (control != '{') {
                throw new Json5Exception("A JSON object must begin with '{'" + lexer);
            }

            GeneralProgramConfigs config = new GeneralProgramConfigs();

            ParseLoop:
            while (true) {
                control = lexer.nextClean();
                switch (control) {
                    case '\0' -> throw new Json5Exception("A JSON object must end with '}'" + lexer);
                    case '}' -> { break ParseLoop;}
                    default -> {
                        lexer.back();
                        memberName = lexer.nextMemberName();
                        control = lexer.nextClean();
                        if (control != ':') throw new Json5Exception(String.format(
                            "Expected ':' after a key, got '%c' instead%s",
                            control,
                            lexer
                        ));

                        value = lexer.nextValue();
                        switch (memberName) {
                            case "Target Version" -> {
                                config.setTargetVersion(Version.valueOf(value.getAsString()));
                                hasTarget = true;
                            }
                            case "Build Version" -> {
                                if (value.isJson5Null()) break;
                                config.setBuildVersion(Version.valueOf(value.getAsString()));
                            }
                            case "Output Path" -> {
                                config.setOutputPath(value.getAsString());
                                hasOutput = true;
                            }
                            case "Build as Zip" ->
                                config.setBuildAsZipped(value.getAsBoolean());
                            case "Exclude" -> {
                                Json5Array array = value.getAsJson5Array();
                                String[] excludes = new String[array.size()];
                                for (int i = 0; i < array.size(); i++) {
                                    excludes[i] = array.get(i).getAsString();
                                }

                                config.setExcludePatterns(excludes);
                            }
                        }

                        control = lexer.nextClean();
                        if (control == '}') break ParseLoop;
                        if (control != ',') throw new Json5Exception(String.format(
                            "Expected ',' or '}' after value, got '%c' instead%s",
                            control,
                            lexer
                        ));
                    }
                }
            }

            if (!hasTarget) throw new InvalidDataException("Missing required property 'Target Version'");
            if (!hasOutput) throw new InvalidDataException("Missing required property 'Output Path'");

            return config;
        }
    }
}
