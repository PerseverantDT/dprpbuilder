package io.github.perseverantdt.config;

import java.nio.file.Path;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import de.marhali.json5.Json5Element;
import de.marhali.json5.exception.Json5Exception;
import de.marhali.json5.stream.Json5Lexer;
import io.github.perseverantdt.exception.InvalidDataException;

public class SpecificProgramConfigs {
    @NotNull String name;
    @Nullable String description;

    @NotNull Path sourcePath;
    @Nullable Path iconPath;
    @Nullable Path readmePath;
    @Nullable Path licensePath;

    public @NotNull String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    public @Nullable String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public @NotNull Path getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(@NotNull Path sourcePath) {
        this.sourcePath = sourcePath;
    }

    public void setSourcePath(@NotNull String sourcePath) {
        this.sourcePath = Path.of(sourcePath);
    }

    public @Nullable Path getIconPath() {
        return iconPath;
    }

    public void setIconPath(@Nullable Path iconPath) {
        this.iconPath = iconPath;
    }

    public void setIconPath(@Nullable String iconPath) {
        if (iconPath == null) this.iconPath = null;
        else this.iconPath = Path.of(iconPath);
    }

    public @Nullable Path getReadmePath() {
        return readmePath;
    }

    public void setReadmePath(@Nullable Path readmePath) {
        this.readmePath = readmePath;
    }

    public void setReadmePath(@Nullable String readmePath) {
        if (readmePath == null) this.readmePath = null;
        else this.readmePath = Path.of(readmePath);
    }

    public @Nullable Path getLicensePath() {
        return licensePath;
    }

    public void setLicensePath(@Nullable Path licensePath) {
        this.licensePath = licensePath;
    }

    public void setLicensePath(@Nullable String licensePath) {
        if (licensePath == null) this.licensePath = null;
        else this.licensePath = Path.of(licensePath);
    }

    public SpecificProgramConfigs() {
        name = "";
        description = null;
        sourcePath = Path.of("src");
        iconPath = null;
        readmePath = null;
        licensePath = null;
    }

    public static class Parser {
        public static SpecificProgramConfigs parse(Json5Lexer lexer) throws InvalidDataException {
            char control;
            String memberName;
            Json5Element value;
            boolean hasName = false;
            boolean hasSource = false;

            control = lexer.nextClean();
            if (control != '{') {
                throw new Json5Exception("A JSON object must begin with '{'" + lexer);
            }

            SpecificProgramConfigs configs = new SpecificProgramConfigs();

            ParseLoop:
            while (true) {
                control = lexer.nextClean();
                switch (control) {
                    case '\0' -> throw new Json5Exception("A JSON object must end with '}'" + lexer);
                    case '}' -> { break ParseLoop; }
                    default -> {
                        lexer.back();
                        memberName = lexer.nextMemberName();
                        control = lexer.nextClean();
                        if (control != ':') throw new Json5Exception(String.format(
                            "Expected ':' after a key, got '%c' instead%s",
                            control,
                            lexer
                        ));

                        value = lexer.nextValue();
                        switch (memberName) {
                            case "Name" -> {
                                configs.setName(value.getAsString());
                                hasName = true;
                            }
                            case "Description" -> {
                                if (value.isJson5Null())
                                    break;
                                configs.setDescription(value.getAsString());
                            }
                            case "Source" -> {
                                configs.setSourcePath(value.getAsString());
                                hasSource = true;
                            }
                            case "Icon" -> {
                                if (value.isJson5Null())
                                    break;
                                configs.setIconPath(value.getAsString());
                            }
                            case "Readme" -> {
                                if (value.isJson5Null())
                                    break;
                                configs.setReadmePath(value.getAsString());
                            }
                            case "License" -> {
                                if (value.isJson5Null())
                                    break;
                                configs.setLicensePath(value.getAsString());
                            }
                        }

                        control = lexer.nextClean();
                        if (control == '}') break ParseLoop;
                        if (control != ',') throw new Json5Exception(String.format(
                            "Expected ',' or '}' after value, got '%c' instead%s",
                            control,
                            lexer
                        ));
                    }
                }
            }

            if (!hasName) throw new InvalidDataException("Missing required property 'Name'");
            if (!hasSource) throw new InvalidDataException("Missing required property 'Source'");

            return configs;
        }
    }
}
