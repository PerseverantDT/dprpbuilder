package io.github.perseverantdt.config;

import java.nio.file.Path;

import org.jetbrains.annotations.NotNull;

import de.marhali.json5.Json5Element;
import de.marhali.json5.exception.Json5Exception;
import de.marhali.json5.stream.Json5Lexer;
import io.github.perseverantdt.exception.InvalidDataException;

public class ProgramConfigs {
    @NotNull GeneralProgramConfigs generalConfigs;
    @NotNull SpecificProgramConfigs datapackConfigs;
    @NotNull SpecificProgramConfigs resourcePackConfigs;

    public @NotNull GeneralProgramConfigs getGeneralConfigs() {
        return generalConfigs;
    }

    public @NotNull SpecificProgramConfigs getDatapackConfigs() {
        return datapackConfigs;
    }

    public @NotNull SpecificProgramConfigs getResourcePackConfigs() {
        return resourcePackConfigs;
    }

    public ProgramConfigs() {
        generalConfigs = new GeneralProgramConfigs();

        datapackConfigs = new SpecificProgramConfigs();
        datapackConfigs.setSourcePath(Path.of("data"));

        resourcePackConfigs = new SpecificProgramConfigs();
        resourcePackConfigs.setSourcePath(Path.of("asset"));
    }

    public static class Parser {
        public static ProgramConfigs parse(Json5Lexer lexer) {
            char control;
            String memberName;
            Json5Element value;
            boolean hasGeneral = false;
            boolean hasDatapack = false;
            boolean hasResource = false;

            control = lexer.nextClean();
            if (control != '{') {
                throw new Json5Exception("A JSON object must begin with '{'" + lexer);
            }

            ProgramConfigs configs = new ProgramConfigs();

            ParseLoop:
            while (true) {
                control = lexer.nextClean();
                switch (control) {
                    case '\0' -> throw new Json5Exception("A JSON object must end with '}'" + lexer);
                    case '}' -> { break ParseLoop;}
                    default -> {
                        lexer.back();
                        memberName = lexer.nextMemberName();
                        control = lexer.nextClean();
                        if (control != ':') throw new Json5Exception(String.format(
                            "Expected ':' after a key, got '%c' instead%s",
                            control,
                            lexer
                        ));

                        switch (memberName) {
                            case "General" -> {
                                configs.generalConfigs = GeneralProgramConfigs.Parser.parse(lexer);
                                hasGeneral = true;
                            }
                            case "Datapack" -> {
                                configs.datapackConfigs = SpecificProgramConfigs.Parser.parse(lexer);
                                hasDatapack = true;
                            }
                            case "Resource Pack" -> {
                                configs.resourcePackConfigs = SpecificProgramConfigs.Parser.parse(lexer);
                                hasResource = true;
                            }
                        }

                        control = lexer.nextClean();
                        if (control == '}') break ParseLoop;
                        if (control != ',') throw new Json5Exception(String.format(
                            "Expected ',' or '}' after value, got '%c' instead%s",
                            control,
                            lexer
                        ));
                    }
                }
            }

            if (!hasGeneral) throw new InvalidDataException("Could not find general configs for builder.");
            if (!hasDatapack) throw new InvalidDataException("Could not find datapack configs for builder.");
            if (!hasResource) throw new InvalidDataException("Could not find resource pack configs for builder.");

            return configs;
        }
    }
}

